/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/app.component.ts
 * @brief
 *
 */

import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';


import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})


export class AppComponent {

  private active: number = 0;

  menu : any[] = [
    {id:0, name: 'Main', link: ''},
    {id:1, name: 'Identities', link: 'identity'},
    {id:2, name: 'Names', link: 'namestore'},
    {id:3, name: 'Peers', link: 'peerstore'},
  ];

  constructor(private router: Router) {
    router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((route: ActivatedRoute) => {
      let index = this.menu.find(x => {
        let url = '/'+x.link;
        return url == route.url+'';
      });
      this.active = this.menu.indexOf(index);
    });
  }

}
