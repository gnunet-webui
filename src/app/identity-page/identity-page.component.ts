/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/identity-page/identity-page.component.ts
 * @brief
 *
 */

import { Component, OnInit } from '@angular/core';
import { Pipe } from "@angular/core";
import { ApiService } from '../api.service';
import { IdentityAPI } from '../identity-api';
import { MessagesService } from '../messages.service';

@Component({
  selector: 'app-identity-page',
  templateUrl: './identity-page.component.html'
})
export class IdentityPageComponent implements OnInit {

  identities: IdentityAPI[];
  filteredItems: IdentityAPI[];
  rename: boolean = false;
  delete: boolean = false;
  addsubsystem: boolean = false;
  changeIdentity: IdentityAPI;
  json: any;
  is_displayed: boolean = true;
  request: boolean = false;
  newsubsystem: string="";

  constructor(private apiService: ApiService,private message: MessagesService) { }

  getAPIs(): void {
    this.request = true;
    this.apiService.getIdentities().subscribe(data => {
      this.identities = data;
      this.request = false;
      this.assignCopy();
    });
  }

  ngOnInit() {
    this.getAPIs();
  }

  assignCopy(){
    this.filteredItems = Object.assign([], this.identities);
  }

  filterItem(value : string){
    this.onReset();
    if(!value) this.assignCopy(); //when nothing has typed
    this.filteredItems = Object.assign([], this.identities).filter(
    item => {
      return ((item.name.indexOf(value) > -1) ||
      (item.pubkey.indexOf(value) > -1))
    });
  }

  onClickRename(identity: IdentityAPI){
    this.is_displayed = false;
    this.rename = true;
    this.changeIdentity = Object.assign({},identity);
  }


  onRename(identity: IdentityAPI){
    this.request = true;
    this.onReset();
    this.filteredItems = [];
    this.json = {'newname':identity.name,'pubkey':identity.pubkey};
    this.apiService.changeIdentity(this.json).subscribe(data => {
      this.message.pushSuccess('Rename was successful.');
      this.getAPIs();
    });
  }

  onReset(){
    this.rename = false;
    this.delete = false;
    this.addsubsystem = false;
    this.is_displayed = true;
  }

  onClickDelete(identity: IdentityAPI){
    this.is_displayed = false;
    this.delete = true;
    this.changeIdentity = Object.assign({},identity);
  }

  onDelete(id:string){
    this.request = true;
    this.onReset();
    this.filteredItems = [];
    this.apiService.deleteIdentity(id).subscribe(data => {
      this.message.pushSuccess('Delete was successful.');
      this.getAPIs();
    });
  }


  onClickAddSubsystem(identity: IdentityAPI){
    this.is_displayed = false;
    this.addsubsystem = true;
    this.changeIdentity = Object.assign({},identity);
  }

  onAddSubsystem(pubkey:string){
    this.request = true;
    this.onReset();
    this.filteredItems = [];
    this.json = {'subsystem':this.newsubsystem,'pubkey':pubkey};
    this.apiService.assignIdentity(this.json).subscribe(data => {
      this.newsubsystem = "";
      this.message.pushSuccess('Subsystem was successfully added.');
      this.getAPIs();
    });
  }

  intFromHash(str:string): number{
    let hash: number = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5)-hash);
       hash = hash & hash;
    }
    return (Math.abs(hash)/4294967295);
  }

  returnHSL(id:string): string{
    return "hsl(" + 360 * this.intFromHash(id) + ',' +
    (90 + 70 * this.intFromHash(id)) + '%,' +
    (85 + 10 * this.intFromHash(id)) + '%)';
  }


}
