/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/app-routing.module.ts
 * @brief
 *
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { filter } from 'rxjs/operators';

import { RestApisComponent } from './rest-apis/rest-apis.component';
import { MainPageComponent } from './main-page/main-page.component';
import { IdentityPageComponent } from './identity-page/identity-page.component';
import { PeerstorePageComponent } from './peerstore-page/peerstore-page.component';
import { NamestorePageComponent } from './namestore-page/namestore-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';

import { MessagesService } from './messages.service';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'apis', component: RestApisComponent },
  { path: 'identity', component: IdentityPageComponent },
  { path: 'peerstore', component: PeerstorePageComponent },
  { path: 'namestore', component: NamestorePageComponent },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
  constructor(private router: Router,
              private messages: MessagesService) {
    router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((route: ActivatedRoute) => {
      this.messages.dismissError();
      this.messages.dismissSuccess();
      this.messages.dismissWarning();
      this.messages.dismissInformation();
    });
  }
}
