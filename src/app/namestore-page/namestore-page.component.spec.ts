/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.
  
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/namestore-page/namestore-page.component.spec.ts
 * @brief 
 *
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamestorePageComponent } from './namestore-page.component';

describe('NamestorePageComponent', () => {
  let component: NamestorePageComponent;
  let fixture: ComponentFixture<NamestorePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamestorePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamestorePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
