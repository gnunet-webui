/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/namestore-page/namestore-page.component.ts
 * @brief
 *
 */

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-namestore-page',
  templateUrl: './namestore-page.component.html'
})
export class NamestorePageComponent implements OnInit {

  gns_name:string;
  gns_recordtype:number = 0;
  private gns_response:any =[];

  private url: string;

  constructor(private apiService:ApiService) { }

  ngOnInit() {
  }

  onSearch(){
    this.url = '?name='+this.gns_name;
    if(this.gns_recordtype != null){
      this.url += '&record_type='+this.gns_recordtype;
    }
    console.log(this.url);
    this.apiService.searchNameSystem(this.url).subscribe(data => {
      this.gns_response = data;
      console.log(data);

    });
  }

}
