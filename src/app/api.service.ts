/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/api.service.ts
 * @brief
 *
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { RestAPI } from './rest-api';
import { IdentityAPI } from './identity-api';
import { catchError, map, tap } from 'rxjs/operators';
import { MessagesService } from './messages.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private restURL = 'http://localhost:7776/'
  private identityURL = this.restURL+'identity';
  private gnsURL = this.restURL+'gns';

  constructor(private http: HttpClient,
              private messages:MessagesService) { }

  getIdentities (): Observable<IdentityAPI[]>{
    return this.http.get<IdentityAPI[]>(this.identityURL)
    .pipe(
      tap(json => this.handleJSON(json)),
      catchError(this.handleError('getIdentities', []))
    );
  }

  createIdentity (json: any): Observable<any>{
    this.messages.dismissError();
    const options = {headers: {'Content-Type': 'application/json'}};
    return this.http.post(this.identityURL, json, options)
    .pipe(
      tap(json => this.handleJSON(json)),
      catchError(this.handleError('createIdentity', []))
    );
  }

  deleteIdentity (id: string): Observable<any>{
    this.messages.dismissError();
    return this.http.delete(this.identityURL+'?pubkey='+id)
    .pipe(
      tap(json => this.handleJSON(json)),
      catchError(this.handleError('deleteIdentity', []))
    );
  }


  changeIdentity (json: any): Observable<any>{
    this.messages.dismissError();
    const options = {headers: {'Content-Type': 'application/json'}};
    return this.http.put(this.identityURL, json, options)
    .pipe(
      tap(json => this.handleJSON(json)),
      catchError(this.handleError('changeIdentity', []))
    );
  }

  assignIdentity (json: any): Observable<any>{
    this.messages.dismissError();
    const options = {headers: {'Content-Type': 'application/json'}};
    return this.http.put(this.identityURL, json, options)
    .pipe(
      tap(json => this.handleJSON(json)),
      catchError(this.handleError('changeIdentity', []))
    );
  }

  searchNameSystem (url: string): Observable<any>{
    this.messages.dismissError();
    return this.http.get(this.gnsURL+'/'+url)
    .pipe(
      tap(json => this.handleJSON(json)),
      catchError(this.handleError('searchNameSystem', []))
    );
  }



  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      //console.error(error); // log to console instead

      if((operation == 'createIdentity') && (error.status == '409'))
      {
        this.messages.pushError('Cannot create identity. Identity already exists.');
      }
      if((operation == 'changeIdentity') && (error.status == '404'))
      {
        this.messages.pushError('Cannot rename identity. Identity not found.');
      }
      if((operation == 'changeIdentity') && (error.status == '409'))
      {
        this.messages.pushError('Cannot rename identity. Identity with this name already exists.');
      }
      if((operation == 'deleteIdentity') && (error.status == '404'))
      {
        this.messages.pushError('Cannot delete identity. Identity not found.');
      }

      if((error.statusText == 'Unknown Error'))
      {
        this.messages.pushError('Unknown Error. Is the server running?');
      }

      return of(result as T);
    };
  }

  private handleJSON (json: any) {
    if(json != null)
    {
      if(json.error != null){
        this.messages.pushError(json.error);
      }
    }
  }
}
