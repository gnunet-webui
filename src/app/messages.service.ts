/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.
  
   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/messages.service.ts
 * @brief 
 *
 */

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private error:string;
  private warning:string;
  private info:string;
  private success:string;

  constructor() { }

  getError() :string{
    return this.error;
  }

  pushError(errorText:string){
    this.error = errorText;
  }

  dismissError(){
    this.error='';
  }

  getWarning() :string{
    return this.warning;
  }

  pushWarning(errorText:string){
    this.warning = errorText;
  }

  dismissWarning(){
    this.warning='';
  }

  getSuccess() :string{
    return this.success;
  }

  pushSuccess(errorText:string){
    this.success = errorText;
  }

  dismissSuccess(){
    this.success='';
  }

  getInformation() :string{
    return this.info;
  }

  pushInformation(errorText:string){
    this.info = errorText;
  }

  dismissInformation(){
    this.info='';
  }
}
