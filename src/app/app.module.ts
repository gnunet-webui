/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/app.module.ts
 * @brief
 *
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RestApisComponent } from './rest-apis/rest-apis.component';
import { AppRoutingModule } from './app-routing.module';
import { MainPageComponent } from './main-page/main-page.component';
import { IdentityPageComponent } from './identity-page/identity-page.component';
import { CreateIdentityComponent } from './create-identity/create-identity.component';
import { MyFilterPipe } from './filter.pipe';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { NamestorePageComponent } from './namestore-page/namestore-page.component';
import { PeerstorePageComponent } from './peerstore-page/peerstore-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';

@NgModule({
  declarations: [
    AppComponent,
    RestApisComponent,
    MainPageComponent,
    IdentityPageComponent,
    CreateIdentityComponent,
    MyFilterPipe,
    ErrorMessageComponent,
    NamestorePageComponent,
    PeerstorePageComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
