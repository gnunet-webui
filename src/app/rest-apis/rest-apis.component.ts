/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/rest-apis/rest-apis.component.ts
 * @brief
 *
 */

import { Component, OnInit } from '@angular/core';
import { RestAPI } from '../rest-api';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-rest-apis',
  templateUrl: './rest-apis.component.html'
})
export class RestApisComponent implements OnInit {

  apis: RestAPI[] = [
    {name: 'Manage Identities', desc: 'Create new identities, edit their names and delete them!', link: 'identity'},
    {name: 'Maintain Names', desc: 'Do things!', link: 'namestore'},
    {name: 'Handle Peers', desc: 'Do things!', link: 'peerstore'},
  ];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

}
