/*
   This file is part of GNUnet.
   Copyright (C) 2012-2015 GNUnet e.V.

   GNUnet is free software: you can redistribute it and/or modify it
   under the terms of the GNU Affero General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNUnet is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/**
 * @author Philippe Buschmann
 * @file src/app/create-identity/create-identity.component.ts
 * @brief
 *
 */

import { Component, OnInit } from '@angular/core';

import { ApiService } from '../api.service';
import { IdentityPageComponent } from '../identity-page/identity-page.component';
import { MessagesService } from '../messages.service';

@Component({
  selector: 'create-identity-component',
  templateUrl: './create-identity.component.html'
})
export class CreateIdentityComponent implements OnInit {

  input_text: string = '';
  private json: any = {'name':''};
  is_free:boolean = true;

  constructor(private apiService: ApiService,
              private message: MessagesService,
              private identity: IdentityPageComponent) { }

  ngOnInit() {
  }

  onClick() {
    if (this.input_text != "" && this.is_free){
      this.is_free = false;
      this.json.name = this.input_text;
      this.input_text = '';
      this.apiService.createIdentity(this.json).subscribe(test => {
        this.message.pushSuccess('Created new identity');
        this.identity.getAPIs();
        this.is_free = true;
      });
    } else {
      this.message.pushError('Missing input. Define name of new object');
    }
  }

}
